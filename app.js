/**
 * Created by ritej on 9/9/2015.
 */

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var config = require('./config');
var formidable = require('formidable');
var gcm = require('node-gcm');
var geolib = require('geolib');


var adminRoutes = express.Router();
var userRoutes = express.Router();


var fs = require('fs');
var Offer = require('./models/offer');
var HH = require('./models/hh');
var Users = require('./models/users');
var Place = require('./models/place');
var Admin = require('./models/admin');


var rand = function () {
    return Math.random().toString(36).substr(2); // remove `0.`
};


var port = process.env.PORT || 3377;


var counterbig = 1;
//var gcmSender = 'AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ';
var counterthumbnail = 1;
var countermenu1 = 1;
var countermenu2 = 1;
var countermenu3 = 1;
var counteroffer = 1;


mongoose.connect(config.database);


app.use(express.static(__dirname + '/uploads'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,token,adminid,userid');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});




app.get('/', function (req, res) {
    res.json({
        message: 'welcome to our api'
    });
});


//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//                                                                                ADMIN ROUTES!!!!!!!!


app.post('/adminlogin', function (req, res) {

    var usrnam = req.body.userName;
    var pass = req.body.password;

    Admin.findOne({
        userName: usrnam,
        password: pass
    }, function (err, user) {
        if (user == null) {
            res.json({
                success: false,
                data: null,
                error: 'Wrong username and password'
            });
        }

        else {

            var token = rand() + rand();
            user.token = token;
            user.save();
            res.json({
                success: true,
                data: {
                    auth_token: token
                }
            });
        }
    });//end of finding admin

});//end of login


//API middleware to check access to admin routes
adminRoutes.use(function (req, res, next) {

    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];

    if (token) {

        // verifies secret and checks exp
        Admin.findOne({
            token: token
        }, function (err, decoded) {
            if (err) {
                return res.json({success: false, data: null});
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });


    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            data: null,
            error: 'token not provided'
        });

    }

});//end of token verification


//API ADMIN route to check if token verified
adminRoutes.get('/', function (req, res) {
    res.json({
        success: true,
        data: {
            message: 'token authenticated'
        }
    });
});//end of token verified or not


//API ADMIN route to add a place to DB
adminRoutes.post('/place', function (req, res) {
    var form = formidable.IncomingForm();

    function rewrite(image, path) {
        fs.readFile(image, function (err, data) {
            fs.writeFile(path, data, function (err) {
                if (err)
                    console.log('error writing');
                fs.unlink(image, function (err) {
                    if (err)
                        console.log('error unlinking');

                    else {
                        console.log('success image saved');
                    }
                });//unlinked temp image
            });//writing to new path
        });//reading image file
    }//end of rewrite function

    form.parse(req, function (err, fields, files) {
        var place = new Place;
        var obj = JSON.parse(fields.placeModel);
        var pathset;


        Place.find({}, function (err, places) {
            if (!err && places) {

                counterbig = (places.length);
                counterthumbnail = (places.length);
                countermenu1 = (places.length);
                countermenu2 = (places.length);
                countermenu3 = (places.length);

                try {
                    counterbig++;
                    var path_big = (__dirname + '/uploads/places/big/big' + counterbig + '.jpg');
                    var big = files.big.path;
                    rewrite(big, path_big);
                    pathset = ('/places/big/big' + counterbig + '.jpg');

                    place.big = pathset;
                }
                catch (err) {
                    console.log(err);
                }

                try {
                    counterthumbnail++;
                    var path_thumbnail = (__dirname + '/uploads/places/thumbnail/thumbnail' + counterthumbnail + '.jpg');
                    var thumbnail = files.thumbnail.path;
                    rewrite(thumbnail, path_thumbnail);
                    pathset = ('/places/thumbnail/thumbnail' + counterthumbnail + '.jpg');

                    place.thumbnail = pathset;
                }
                catch (err) {
                    console.log(err);
                }

                try {
                    countermenu1++;
                    var path_menu1 = (__dirname + '/uploads/places/menu/menu1' + countermenu1 + '.jpg');

                    var menu1 = files.menu1.path;
                    rewrite(menu1, path_menu1);
                    pathset = ('/places/menu/menu1' + countermenu1 + '.jpg');

                    place.menu1 = pathset;
                }
                catch (err) {
                    console.log(err);
                }


                try {
                    countermenu2++;
                    var path_menu2 = (__dirname + '/uploads/places/menu/menu2' + countermenu2 + '.jpg');
                    var menu2 = files.menu2.path;
                    rewrite(menu2, path_menu2);
                    pathset = ('/places/menu/menu2' + countermenu2 + '.jpg');

                    place.menu2 = pathset;
                }
                catch (err) {
                    console.log(err);

                }

                try {
                    countermenu3++;
                    var path_menu3 = (__dirname + '/uploads/places/menu/menu3' + countermenu3 + '.jpg');
                    var menu3 = files.menu3.path;
                    rewrite(menu3, path_menu3);
                    pathset = ('/places/menu/menu3' + countermenu3 + '.jpg');
                    place.menu3 = pathset;
                }
                catch (err) {
                    console.log(err);

                }


                place.address = obj.address;
                place.locality = obj.locality;
                place.placeName = obj.placeName;
                place.phoneNumber = obj.phoneNumber;
                place.startTime = obj.startTime;
                place.endTime = obj.endTime;
                place.lat = obj.lat;
                place.lon = obj.lon;

                place.save(function (err, data) {
                    if (!err && data) {
                        res.json({
                            success: true,
                            data: place,
                            error: null
                        });
                    } else res.json({
                        success: false,
                        data: null,
                        error: 'an error occured'
                    });
                });//save place
            } else {
                try {
                    path_big = (__dirname + '/uploads/places/big/big' + counterbig + '.jpg');
                    big = files.big.path;
                    rewrite(big, path_big);
                    pathset = ('/places/big/big' + counterbig + '.jpg');

                    place.big = pathset;
                }
                catch (err) {
                    console.log(err);
                }

                try {
                    path_thumbnail = (__dirname + '/uploads/places/thumbnail/thumbnail' + counterthumbnail + '.jpg');
                    thumbnail = files.thumbnail.path;
                    rewrite(thumbnail, path_thumbnail);
                    pathset = ('/places/thumbnail/thumbnail' + counterthumbnail + '.jpg');

                    place.thumbnail = pathset;
                }
                catch (err) {
                    console.log(err);
                }

                try {
                    path_menu1 = (__dirname + '/uploads/places/menu/menu1' + countermenu1 + '.jpg');
                    menu1 = files.menu1.path;
                    rewrite(menu1, path_menu1);
                    pathset = ('/places/menu/menu1' + countermenu1 + '.jpg');

                    place.menu1 = pathset;
                }
                catch (err) {
                    console.log(err);
                }


                try {
                    path_menu2 = (__dirname + '/uploads/places/menu/menu2' + countermenu2 + '.jpg');
                    menu2 = files.menu2.path;
                    rewrite(menu2, path_menu2);
                    pathset = ('/places/menu/menu2' + countermenu2 + '.jpg');

                    place.menu2 = pathset;
                }
                catch (err) {
                    console.log(err);

                }

                try {
                    path_menu3 = (__dirname + '/uploads/places/menu/menu3' + countermenu3 + '.jpg');
                    menu3 = files.menu3.path;
                    rewrite(menu3, path_menu3);
                    pathset = ('/places/menu/menu3' + countermenu3 + '.jpg');
                    place.menu3 = pathset;
                }
                catch (err) {
                    console.log(err);

                }


                place.address = obj.address;
                place.locality = obj.locality;
                place.placeName = obj.placeName;
                place.phoneNumber = obj.phoneNumber;
                place.startTime = obj.startTime;
                place.endTime = obj.endTime;
                place.lat = obj.lat;
                places.lon = obj.lon;
                place.save(function (err, data) {
                    if (!err && data) {
                        res.json({
                            success: true,
                            data: place,
                            error: null
                        });
                    } else res.json({
                        success: false,
                        data: null,
                        error: 'an error occured'
                    });
                });//save place
            }
        });
    });//end of form parse
});//end of place post


//API ADMIN route to return placeS LIST
adminRoutes.get('/places', function (req, res) {
    Place.find({}, {'placeName': 1, thumbnail: 1}, function (err, places) {
        if (!err && places) {
            res.json({
                success: true,
                data: {
                    places: places
                },
                error: null
            });
            console.log(places);
        } else {
            res.json({
                success: false,
                data: null,
                error: 'nothing to show!'
            });
        }
    });//end of place find

});//end of get places


//API ADMIN routes to add an offer
adminRoutes.post('/offer', function (req, res) {
    var form = formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        var offers = new Offer;

        function rewrite(image, path) {
            fs.readFile(image, function (err, data) {
                fs.writeFile(path, data, function (err) {
                    if (err)
                        console.log('error writing');
                    fs.unlink(image, function (err) {
                        if (err)
                            console.log('error unlinking');

                        else {
                            console.log('success image saved');
                        }
                    });//unlinked temp image
                });//writing to new path
            });//reading image file
        }//end of rewrite function
        var offerobj = JSON.parse(fields.offerModel);
        var offerDays = [];
        var alcoholType = [];
        var i;

        Offer.find({}, function (err, offer) {
            if (!err && offer) {
                counteroffer = offer.length;
                for (i = 0; i < offerobj.offerDays.length; i++) {

                    offerDays.push(offerobj.offerDays[i]);
                }




                for (i = 0; i < offerobj.offerAlcoholType.length; i++) {
                    alcoholType.push(offerobj.offerAlcoholType[i]);
                }


                try {


                    counteroffer++;
                    var path_offerImage = (__dirname + '/uploads/offers/offerimage' + counteroffer + '.jpg');
                    var offerImage = files.offerImage.path;
                    var pathset = ('/offers/offerimage' + counteroffer + '.jpg');

                    rewrite(offerImage, path_offerImage);
                    offers.offerImage = pathset;


                }
                catch (err) {
                    console.log(err);
                }
                offers.offerName = offerobj.offerName;
                offers.offerDetail = offerobj.offerDetail;
                offers.offerDays = offerDays;
                offers.startTime = offerobj.startTime;
                offers.endTime = offerobj.endTime;
                offers.offerAlcoholType = alcoholType;

                offers.save(function (err, data) {


                    if (!err && data) {

                        var message = new gcm.Message({
                            collapseKey: 'demo',
                            priority: 'normal',
                            contentAvailable: true,
                            delayWhileIdle: true,
                            timeToLive: 3,
                            //restrictedPackageName: "somePackageName",
                            data: {
                                offer: 'abc'
                            },
                            notification: {
                                title: "Hello!!",
                                icon: "ic_launcher",
                                body: "This is a notification that will be displayed ASAP."
                            }
                        });

                        var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');

                        Users.find({}, {gcmId: 1, _id: 0}, function (err, data) {
                            var registrationIds = [];
                            for (var i = 0; i < data.length; i++) {
                                registrationIds.push(data[i].gcmId);
                            }
                            console.log(data);
                            sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                                if (err) console.log(err);
                                else console.log(result);
                            });
                        });


                    }
                });//save offer

                for (i = 0; i < offerobj.places.length; i++) {


                    Place.findOne({placeName: offerobj.places[i].placeName}, function (err, data1) {
                        var obj = {};
                        obj.placeName = null;
                        obj.placeId = null;
                        obj.placeImage = null;
                        obj.checked = true;

                        obj.placeName = data1.placeName;
                        obj.placeImage = data1.thumbnail;
                        offers.Places.push(obj);
                        offers.save(function (err, data) {
                            if (!err && data) {
                                if (!err && data) {
                                    res.json({
                                        success: true,
                                        data: {message: 'offer saved'},
                                        error: null
                                    });
                                }
                            }
                            else {
                                res.json({
                                    success: false,
                                    data: null,
                                    error: 'error saving offer'
                                });
                            }
                        });
                    });


                }

            } else {
                counteroffer = 0;
                for (i = 0; i < offerobj.offerDays.length; i++) {

                    offerDays.push(offerobj.offerDays[i]);
                }




                for (i = 0; i < offerobj.offerAlcoholType.length; i++) {
                    alcoholType.push(offerobj.offerAlcoholType[i]);
                }


                try {


                    counteroffer++;
                    path_offerImage = (__dirname + '/uploads/offers/offerimage' + counteroffer + '.jpg');
                    offerImage = files.offerImage.path;
                    pathset = ('/offers/offerimage' + counteroffer + '.jpg');

                    rewrite(offerImage, path_offerImage);
                    offers.offerImage = pathset;


                }
                catch (err) {
                    console.log(err);
                }
                offers.offerName = offerobj.offerName;
                offers.offerDetail = offerobj.offerDetail;
                offers.offerDays = offerDays;
                offers.startTime = offerobj.startTime;
                offers.endTime = offerobj.endTime;
                offers.offerAlcoholType = alcoholType;

                offers.save(function (err, data) {


                    if (!err && data) {


                        var message = new gcm.Message({
                            collapseKey: 'demo',
                            priority: 'normal',
                            contentAvailable: true,
                            delayWhileIdle: true,
                            timeToLive: 3,
                            //restrictedPackageName: "somePackageName",
                            data: {
                                offer: 'abc'
                            },
                            notification: {
                                title: "Hello!!",
                                icon: "ic_launcher",
                                body: "This is a notification that will be displayed ASAP."
                            }
                        });

                        var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');

                        Users.find({}, {gcmId: 1, _id: 0}, function (err, data) {
                            var registrationIds = [];
                            for (var i = 0; i < data.length; i++) {
                                registrationIds.push(data[i].gcmId);
                            }
                            console.log(data);
                            sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                                if (err) console.log(err);
                                else console.log(result);
                            });
                        });


                    }
                });//save offer

                for (i = 0; i < offerobj.places.length; i++) {


                    Place.findOne({placeName: offerobj.places[i].placeName}, function (err, data1) {
                        var obj = {};
                        obj.placeName = null;
                        obj.placeId = null;
                        obj.placeImage = null;
                        obj.checked = true;

                        obj.placeName = data1.placeName;
                        obj.placeImage = data1.placeImage.thumbnail;
                        offers.Places.push(obj);
                        offers.save(function (err, data) {
                            if (!err && data) {
                                if (!err && data) {
                                    res.json({
                                        success: true,
                                        data: {message: 'offer saved'},
                                        error: null
                                    });
                                }
                            }
                            else {
                                res.json({
                                    success: false,
                                    data: null,
                                    error: 'error saving offer'
                                });
                            }
                        });
                    });


                }

            }
        });


    });//end of form parse
});//end of offers post


//API ADMIN route to add a happy hour detail
adminRoutes.post('/happyhour', function (req, res) {
    var form = formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (!err && fields && files) {

            var hhobj = JSON.parse(fields.happyHourModel);
            var hh = new HH;
            var alcoholType = [];
            var days = [];

            for (var i = 0; i < hhobj.happyHourAlcoholType.length; i++) {
                alcoholType.push(hhobj.happyHourAlcoholType[i]);
            }
            for (i = 0; i < hhobj.happyHourDays.length; i++) {
                days.push(hhobj.happyHourDays[i]);
            }

            hh.happyHourAlcoholType = alcoholType;
            hh.happyHourDescription = hhobj.happyHourDescription;
            hh.happyHourStartTime = hhobj.happyHourStartTime;
            hh.happyHourEndTime = hhobj.happyHourEndTime;
            hh.happyHourDays = days;
            hh.save(function (err, data) {
                if (!err && data) {
                    res.json({
                        success: true,
                        data: fields,
                        error: null
                    });
                } else {
                    res.json({
                        success: false,
                        data: null,
                        error: 'error occured'
                    });
                }
            });//save hh model


            for (i = 0; i < hhobj.places.length; i++) {

                Place.findOne({placeName: hhobj.places[i].placeName}, function (err, placefound) {

                    var obj = {};
                    obj.placeName = null;
                    obj.placeId = null;
                    obj.placeImage = null;
                    obj.checked = true;
                    obj.lat = null;
                    obj.lon = null;
                    obj.lat = placefound.lat;
                    obj.lon = placefound.lon;
                    obj.placeName = placefound.placeName;
                    obj.placeImage = placefound.big;
                    hh.Places.push(obj);
                    hh.save();
                });
            }


        }


    });//end of form parse

});//end of happy hours post


//API to edit and update a place
adminRoutes.put('/editplacedetail', function (req, res) {


    var form = formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (!err && fields && files) {

            var editplaceobj = fields.placeModel;
            var placeName = editplaceobj.placeName;
            Place.findOne({placeName:placeName}, function (err, placefound) {
                if(!err && placefound){
                    placefound = editplaceobj;
                    placefound.save(function (err, placeupdated) {
                        if(!err && placeupdated){
                            res.json({
                                success:true,
                                data:{
                                    message: 'Place updated'
                                },
                                error: null
                            });
                        }
                    });
                }
            });

        }else{
            res.json({
                success:false,
                data:null,
                error: ' Error parsing form '
            });
        }
    });
    //var offers = [];
    //var happyhours = [];
    //
    //function getoffers(callback) {
    //    Offer.find({placeName: placeName}, function (err, offersfound) {
    //        if (!err && offersfound) {
    //            offers.push(offersfound);
    //            callback(null, offers);
    //        } else {
    //            callback(err, null);
    //        }
    //    });
    //}
    //
    //function getHappyHours(callback2) {
    //    HH.findOne({placeName: placeName}, function (err, hhfound) {
    //        if (!err && hhfound) {
    //            try{
    //
    //            }
    //
    //            happyhours.push(hhfound);
    //            callback2(null, happyhours);
    //        } else {
    //            callback2(err, null);
    //        }
    //    });
    //}

});


//API to edit place happyhours
adminRoutes.put('/editplacehappyhour', function (req, res) {
    var form = formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if(!err && fields && files){
            var edithhobj = fields.happyHourModel;
            HH.findOne({placeName: edithhobj.placeName}, function (err, hhfound) {
                if(!err && hhfound){
                    hhfound = edithhobj;
                    hhfound.save(function (err, hhsaved) {
                        if(!err && hhsaved){
                            res.json({
                                success: true,
                                data:{
                                    message: 'Happy Hour Updated'
                                },
                                error:null
                            });
                        }
                    });
                }
            });
        }else{
            res.json({
                success:false,
                data:null,
                error: 'Error parsing incoming form'
            });
        }
    });//end of form parse
});//end of edit HH


//API to edit place happyhours
adminRoutes.put('/editplaceoffer', function (req, res) {
    var form = formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if(!err && fields && files){
            var editofferobj = fields.happyHourModel;
            Offer.findOne({placeName: editofferobj.placeName, offerName: editofferobj.offerName}, function (err, offerfound) {
                if(!err && offerfound){
                    hhfound = edithhobj;
                    hhfound.save(function (err, hhsaved) {
                        if(!err && hhsaved){
                            res.json({
                                success: true,
                                data:{
                                    message: 'Happy Hour Updated'
                                },
                                error:null
                            });
                        }
                    });
                }
            });
        }else{
            res.json({
                success:false,
                data:null,
                error: 'Error parsing incoming form'
            });
        }
    });//end of form parse
});//end of edit HH






//API ADMIN for autocomplete query
adminRoutes.get('/search', function (req, res) {
    var query = req.params.placeName || req.query.placeName;
    Place.find({'placeName': new RegExp('^' + query, 'i')}, {placeName: 1}, function (err, placelist) {
        console.log(query);
        if (!err && placelist) {
            res.json({
                success: true,
                data: {
                    places: placelist
                },
                error: null
            });
        } else {
            res.json({
                success: false,
                data: null,
                error: 'place not found'
            });
        }
    }).sort({'placeName': 1});
});//end of autocomplete query


//API ADMIN route to return place selected details,offers and happy hours
adminRoutes.get('/searchresult', function (req, res) {
    var placename = req.param.placeName || req.query.placeName;


    Place.findOne({'placeName': placename}, function (err, placefound) {

        if (!err && placefound) {

            res.json({
                success: true,
                data: placefound,
                error: null

            });

        } else {
            res.json({
                success: false,
                data: null,
                error: 'place does not exist'
            });
        }
    });//end of place query

});//end of admin search return


//API to update places
adminRoutes.put('/updateplace', function (req, res) {
    var placeName = req.body.placeName;
    Place.findOne({placeNme: placeName}, function (err, placefound) {
        if (!err && placefound) {

        }
    });
});


//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//                                                                                   USER ROUTES!!!!
//User login API


app.post('/login', function (req, res) {
    var form = formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (!err && fields && files) {
            var userobj = JSON.parse(fields.userDto);
            var users = new Users;
            //console.log(userobj);
            Users.findOne({loginId: userobj.loginId}, function (err, userFound) {
                if (!err && userFound) {


                    if (userobj.gcmId != null) {
                        Users.update({loginId: userobj.loginId}, {
                            gcmId: userobj.gcmId
                        }, function (err) {
                            if (err) console.log(err);
                        });
                    }

                    if (userobj.name != null) {
                        Users.update({loginId: userobj.loginId}, {
                            name: userobj.name
                        }, function (err) {
                            if (err) console.log(err);
                        });
                    }

                    if (userobj.gender != null) {
                        Users.update({loginId: userobj.loginId}, {
                            gender: userobj.gender
                        }, function (err) {
                            if (err) console.log(err);
                        });
                    }

                    if (userobj.birthday != null) {
                        Users.update({loginId: userobj.loginId}, {
                            birthday: userobj.birthday
                        }, function (err) {
                            if (err) console.log(err);
                        });
                    }

                    if (userobj.profilePic != null) {
                        Users.update({loginId: userobj.loginId}, {
                            profilePic: userobj.profilePic
                        }, function (err) {
                            if (err) console.log(err);
                        });
                    }

                    if (userobj.email != null) {
                        Users.update({loginId: userobj.loginId}, {
                            email: userobj.email
                        }, function (err) {
                            if (err) console.log(err);
                        });
                    }


                    userobj.friendsList.forEach(function (eachFriend) {
                        var friendName = eachFriend.name;
                        var friendId = eachFriend.loginId;
                        var friendProfilePic = eachFriend.profilePic;
                        var loginid = userobj.loginId;
                        Users.findOne({loginId: loginid}, function (err, found) {
                            if (!err && (found.friendsList.length > 0)) {
                                var existingfriends;
                                existingfriends = found.friendsList;
                                var exists = false;
                                existingfriends.forEach(function (eachExistingFriend) {
                                    if(eachFriend.loginId == eachExistingFriend.loginId){
                                        exists = true;
                                    }
                                });

                                if(exists == true){

                                }else{
                                    var now = new Date();
                                    var obj = {};
                                    obj.name = null;
                                    obj.loginId = null;
                                    obj.profilePic = null;
                                    obj.status = null;
                                    obj.status = 'new';

                                    obj.name = friendName;
                                    obj.loginId = friendId;
                                    obj.profilePic = friendProfilePic;
                                    obj.created_at = now;
                                    obj.updated_at = now;
                                    Users.update({loginId: loginid}, {
                                        $push: { friendsList: obj }
                                    },{safe:true, upsert:true}, function (err, friendsaved) {
                                        if (!err && friendsaved) {

                                        }
                                        //console.log(friendsaved);
                                    });
                                }

                                //if (existingfriends.indexOf(friendId) > -1) {
                                //    //if in the array
                                //} else {
                                //    var now = new Date();
                                //    var obj = {};
                                //    obj.name = null;
                                //    obj.loginId = null;
                                //    obj.profilePic = null;
                                //    obj.status = null;
                                //    obj.status = 'new';
                                //
                                //    obj.name = friendName;
                                //    obj.loginId = friendId;
                                //    obj.profilePic = friendProfilePic;
                                //    obj.created_at = now;
                                //    obj.updated_at = now;
                                //    Users.update({loginId: loginid}, {
                                //        push: {friendsList: obj}
                                //    }, function (err, friendsaved) {
                                //        if (!err && friendsaved) {
                                //
                                //        }
                                //        //console.log(friendsaved);
                                //    });
                                //}


                            }//end of if(!err && found.friendsList.length>0)
                            else {
                                var now1 = new Date();
                                var obj1 = {};
                                obj1.name = null;
                                obj1.loginId = null;
                                obj1.profilePic = null;
                                obj1.status = null;
                                obj1.status = 'new';
                                obj1.name = friendName;
                                obj1.loginId = friendId;

                                obj1.profilePic = friendProfilePic;
                                obj1.created_at = now1;
                                obj1.updated_at = now1;
                                Users.findOneAndUpdate({loginId: loginid}, {
                                    $push: {friendsList: obj1}
                                },{safe:true, upsert:true}, function (err, friendsaved1) {
                                    if (!err && friendsaved1) {

                                    }
                                    //console.log(friendsaved);
                                });
                            }
                        });
                    });
                    //
                    //for (var l = 0; l < userobj.friendsList.length; l++) {
                    //
                    //    var friendName = userobj.friendsList[l].name;
                    //    var friendId = userobj.friendsList[l].loginId;
                    //    var friendProfilePic = userobj.friendsList[l].profilePic;
                    //    var loginid = userobj.loginId;
                    //    Users.findOne({loginId: loginid}, function (err, found) {
                    //        if (!err && (found.friendsList.length > 0)) {
                    //            var existingfriends;
                    //            existingfriends = found.friendsList;
                    //
                    //            if (existingfriends.indexOf(friendId) > -1) {
                    //                //if in the array
                    //            } else {
                    //                var now = new Date();
                    //                var obj = {};
                    //                obj.name = null;
                    //                obj.loginId = null;
                    //                obj.profilePic = null;
                    //                obj.status = null;
                    //                obj.status = 'new';
                    //
                    //                obj.name = friendName;
                    //                obj.loginId = friendId;
                    //                obj.profilePic = friendProfilePic;
                    //                obj.created_at = now;
                    //                obj.updated_at = now;
                    //                Users.update({loginId: loginid}, {
                    //                    push: {friendsList: obj}
                    //                }, function (err, friendsaved) {
                    //                    if (!err && friendsaved) {
                    //
                    //                    }
                    //                    //console.log(friendsaved);
                    //                });
                    //            }
                    //
                    //
                    //        }//end of if(!err && found.friendsList.length>0)
                    //        else {
                    //            var now1 = new Date();
                    //            var obj1 = {};
                    //            obj1.name = null;
                    //            obj1.loginId = null;
                    //            obj1.profilePic = null;
                    //            obj1.status = null;
                    //            obj1.status = 'new';
                    //            obj1.name = friendName;
                    //            obj1.loginId = friendId;
                    //
                    //            obj1.profilePic = friendProfilePic;
                    //            obj1.created_at = now1;
                    //            obj1.updated_at = now1;
                    //            Users.findOneAndUpdate({loginId: loginid}, {
                    //                push: {friendsList: obj1}
                    //            }, function (err, friendsaved1) {
                    //                if (!err && friendsaved1) {
                    //
                    //                }
                    //                //console.log(friendsaved);
                    //            });
                    //        }
                    //    });
                    //}


                    var token = rand() + rand();
                    var now = new Date();
                    Users.findOneAndUpdate({loginId: userobj.loginId}, {
                        token: token,
                        updated_at: now
                    }, function (err, data) {
                        if (!err && data) {
                            res.json({
                                success: true,
                                data: {
                                    auth_token: token
                                },
                                error: null
                            });
                        } else {
                            res.json({
                                success: false,
                                data: null,
                                error: 'An error occured, no token saved'
                            });
                        }
                    });

                } else {


                    var token1 = rand() + rand();
                    var now1 = new Date();
                    users.loginId = userobj.loginId;
                    users.name = userobj.name;
                    users.gender = userobj.gender;
                    users.email = userobj.email;
                    users.birthday = userobj.birthday;
                    users.profilePic = userobj.profilePic;
                    users.gcmId = userobj.gcmId;
                    users.token = token1;
                    users.created_at = now1;
                    users.updated_at = now1;

                    users.save(function (err, data) {
                        if (!err && data) {
                            res.json({
                                success: true,
                                data: {
                                    auth_token: token1
                                },
                                error: null
                            });
                        } else {
                            res.json({
                                success: false,
                                data: null,
                                error: 'error while saving new user'
                            });
                        }
                    });

                    userobj.friendsList.forEach(function (eachFriend) {
                        var obj1 = {};
                        var now2 = new Date();
                        obj1.name = null;
                        obj1.loginId = null;
                        obj1.profilePic = null;
                        obj1.status = 'new';
                        if (!obj1.created_at) {
                            obj1.created_at = now2;
                        }
                        obj1.updated_at = now2;

                        obj1.name = eachFriend.name;
                        obj1.placeId = eachFriend.loginId;
                        obj1.profilePic = eachFriend.profilePic;
                        obj1.loginId = eachFriend.loginId;
                        users.friendsList.push(obj1);
                        users.save(function (err, friendItemSaved) {
                            if(!err && friendItemSaved){
                                var obj3 = {};
                                var now3 = new Date();
                                obj3.name = null;
                                obj3.loginId = null;
                                obj3.profilePic = null;
                                obj3.status = 'new';
                                if (!obj3.created_at) {
                                    obj3.created_at = now3;
                                }
                                obj3.updated_at = now3;

                                obj3.name = friendItemSaved.name;
                                obj3.placeId = friendItemSaved.loginId;
                                obj3.profilePic = friendItemSaved.profilePic;
                                obj3.loginId = friendItemSaved.loginId;
                                Users.findOneAndUpdate({loginId: eachFriend.loginId},{
                                    $push:{friendsList:obj3}
                                },{safe:true, upsert:true}, function (err, updated) {
                                    if(!err && updated){

                                        var message = new gcm.Message({
                                            collapseKey: 'demo',
                                            priority: 'high',
                                            contentAvailable: true,
                                            delayWhileIdle: true,
                                            timeToLive: 3,
                                            //restrictedPackageName: "somePackageName",
                                            data: {
                                                type: "newfriend",
                                                title: "New friend joined!",
                                                body: "Your friend "+ friendItemSaved.name + "just joined Boozkey",
                                                newFriend: friendItemSaved
                                            },
                                            notification: {
                                                icon: "app_logo",
                                                sound: "default",
                                                color: "#fbb117"
                                            }
                                        });

                                        //setting sender registered API KEY
                                        var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');
                                        var registrationIds = [];
                                        //adding sender reg Id (gcmId)
                                        registrationIds.push(updated.gcmId);
                                        sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                                            if (!err && result) {

                                            }
                                        });//end of push gcm send request

                                    }
                                });
                            }
                        });

                    });

                    //for (var i = 0; i < userobj.friendsList.length; i++) {
                    //    var obj1 = {};
                    //    var now2 = new Date();
                    //    obj1.name = null;
                    //    obj1.loginId = null;
                    //    obj1.profilePic = null;
                    //    obj1.status = 'new';
                    //    if (!obj1.created_at) {
                    //        obj1.created_at = now2;
                    //    }
                    //    obj1.updated_at = now2;
                    //
                    //    obj1.name = userobj.friendsList[i].name;
                    //    obj1.placeId = userobj.friendsList[i].loginId;
                    //    obj1.profilePic = userobj.friendsList[i].profilePic;
                    //    obj1.loginId = userobj.friendsList[i].loginId;
                    //    users.friendsList.push(obj1);
                    //    users.save(function (err, friendItemSaved) {
                    //        if(!err && friendItemSaved){
                    //            var obj3 = {};
                    //            var now3 = new Date();
                    //            obj3.name = null;
                    //            obj3.loginId = null;
                    //            obj3.profilePic = null;
                    //            obj3.status = 'new';
                    //            if (!obj3.created_at) {
                    //                obj3.created_at = now3;
                    //            }
                    //            obj3.updated_at = now3;
                    //
                    //            obj3.name = friendItemSaved.name;
                    //            obj3.placeId = friendItemSaved.loginId;
                    //            obj3.profilePic = friendItemSaved.profilePic;
                    //            obj3.loginId = friendItemSaved.loginId;
                    //            Users.findOneAndUpdate({loginId: userobj.friendsList[i].loginId},{
                    //                $push:{friendsList:obj3}
                    //            },{safe:true, upsert:true}, function (err, updated) {
                    //                if(!err && updated){
                    //
                    //                    var message = new gcm.Message({
                    //                        collapseKey: 'demo',
                    //                        priority: 'high',
                    //                        contentAvailable: true,
                    //                        delayWhileIdle: true,
                    //                        timeToLive: 3,
                    //                        //restrictedPackageName: "somePackageName",
                    //                        data: {
                    //                            type: "newfriend",
                    //                            title: "New friend joined!",
                    //                            body: "Your friend "+ friendItemSaved.name + "just joined Boozkey",
                    //                            newFriend: friendItemSaved
                    //                        },
                    //                        notification: {
                    //                            icon: "app_logo",
                    //                            sound: "default",
                    //                            color: "#fbb117"
                    //                        }
                    //                    });
                    //
                    //                    //setting sender registered API KEY
                    //                    var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');
                    //                    var registrationIds = [];
                    //                    //adding sender reg Id (gcmId)
                    //                    registrationIds.push(updated.gcmId);
                    //                    sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                    //                        if (!err && result) {
                    //
                    //                        }
                    //                    });//end of push gcm send request
                    //
                    //                }
                    //            });
                    //        }
                    //    });
                    //
                    //}

                }//end of else


            }); //end of user findone

        }//end of error check for form parse
        else {
            res.json({
                success: false,
                data: null,
                error: 'An error occurred or fields or files null!'
            });
            console.log(err);
        }

    });//end of form parse
});//end of POST user login


//MIDDLEWARE to verify incoming auth_token '/user'
userRoutes.use(function (req, res, next) {

    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];

    if (token) {

        // verifies secret and checks exp
        Users.findOne({
            token: token
        }, function (err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    data: null,
                    error: 'No user with that token'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });


    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            data: null,
            error: 'token not provided'
        });

    }

});//end of token verification


//API USER route to check if token verified
userRoutes.get('/', function (req, res) {
    res.json({
        success: true,
        data: {
            message: 'token authenticated'
        }
    });
});//end of token verified or not


//API for user routes to retrieve all places list
userRoutes.get('/places', function (req, res) {

    Place.find({}, {_id: 0, placeName: 1, thumbnail: 1}).sort({placeName: 1}).exec(function (err, places) {
        if (!err && places) {
            res.json({
                success: true,
                data: {
                    places: places
                },
                error: null
            });
            console.log(places);
        } else {
            res.json({
                success: false,
                data: null,
                error: 'nothing to show!'
            });
        }
    }); //end of place find
});//end of get places


//API get call for autocomplete search query USER
userRoutes.get('/search', function (req, res) {
    var query = req.params.placeName || req.query.placeName;
    Place.find({'placeName': new RegExp('^' + query, 'i')}, {placeName: 1, _id: 0}, function (err, placelist) {
        console.log(query);
        if (!err && placelist) {
            res.json({
                success: true,
                data: {
                    places: placelist
                },
                error: null
            });
        } else {
            res.json({
                success: false,
                data: null,
                error: 'place not found'
            });
        }
    }).sort({'placeName': 1});//end of place find by regular expression
});//end of autocomplete query


//API to return place details on query
userRoutes.get('/searchresult', function (req, res) {
    var placename = req.param.placeName || req.query.placeName;

    Place.findOne({'placeName': placename}, function (err, placefound) {

        if (!err && placefound) {

            res.json({
                success: true,
                data: placefound,
                error: null

            });

        } else {
            res.json({
                success: false,
                data: null,
                error: 'place does not exist'
            })
        }
    });//end of offer query

});//query results response back to user


//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//                                                                                          OFFER ROUTES
//API user route for OFFERS page populate
userRoutes.get('/offers', function (req, res) {
    Offer.find({}, {
        '_id': 0,
        '__v': 0,
        'Places._id': 0,
        'Places.checked': 0,
        'Places.placeId': 0,
        'Places.lat': 0,
        'Places.lon': 0
    }, function (err, offers) {
        if (!err && offers) {
            res.json({
                success: true,
                data: {
                    offers: offers
                },
                error: null
            });
        } else {
            res.json({
                success: false,
                data: null,
                error: 'wierdly there are no offers to show!'
            });
        }
    });
});//end of user api for offers


//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//                                                                              HAPPY HOUR ROUTES
//API user route for happy hours page populate
userRoutes.get('/happyhours', function (req, res) {
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    Users.findOne({token: token}, function (err, userfound) {
        if (null == userfound.likedPlaces) {
            var likedPlaces = null;
        } else {
            likedPlaces = userfound.likedPlaces;
        }

        HH.find({}, function (err, happyhours) {
            if (!err && happyhours) {
                if (null != likedPlaces) {

                    var obj = {
                        happyHours: happyhours,
                        likedPlaces: likedPlaces
                    };
                }
                else {
                    obj = {
                        happyHours: happyhours,
                        likedPlaces: null
                    };
                }
                res.json({
                    success: true,
                    data: obj,
                    error: null

                });

                console.log('HH LIST SENT');
            } else {
                res.json({
                    success: false,
                    data: null,
                    error: 'wierdly there are no offers to show!'
                });
            }

        });
    });
});//end of user api for HAPPY HOURS


//API route to like a place
userRoutes.put('/likeplace', function (req, res) {
    var placename = req.query.placeName;
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    Place.findOne({placeName: placename}, function (err, placedetails) {
        if (!err && placedetails) {
            var obj = {};
            obj.placeName = null;
            obj.placeName = placedetails.placeName;
            Users.findOneAndUpdate({token: token}, {
                $push: {likedPlaces: obj}
            }, function (err, likesupdated) {
                if (!err && likesupdated) {
                    res.json({
                        success: true,
                        data: {
                            message: 'user likes updated'
                        },
                        error: null
                    });
                } else {
                    res.json({
                        success: false,
                        data: null,
                        error: 'Like not registered with user'
                    });
                }
            });//end of user findOne for user liked list update
        } else console.log(err);
    });//end of place findOne
});//end of API route to likea place


//API route to remove like of a place
userRoutes.put('/unlikeplace', function (req, res) {
    var placename = req.query.placeName;
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];

    Users.findOneAndUpdate({token: token, 'likedPlaces.placeName': placename}, {
        $pull: {likedPlaces: {placeName: placename}}
    }, function (err, likeremoved) {
        if (!err && likeremoved) {
            res.json({
                success: true,
                data: {
                    message: 'place removed from user likedPlaces list'
                },
                error: null
            });
        } else
            res.json({
                success: false,
                data: null,
                error: 'error removing place from likedPlaces list'
            });
    });
});//end of place like removal API


//API route to get HH details
userRoutes.get('/happyhourdetail', function (req, res) {
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    var placename = req.query.placeName;

    Place.findOne({placeName: placename}, function (err, placefound) {
        if (!err && placefound) {
            res.json({
                success: true,
                data: {
                    places: placefound
                },
                error: null
            });
        } else {
            res.json({
                success: false,
                data: null,
                error: 'no place mathcing that name!'

            });
        }
    });

});


userRoutes.get('/mapfriendscheckin', function (req, res) {
    var place = req.query.placeName;
    var token = req.headers['auth_token'];


    var friendsWithCheckin = [];

    Users.findOne({token: token}, function (err, currentuser) {
        if (!err && currentuser) {
            var friends = currentuser.friendsList;

            Users.find({'checkIns.placeName': place}, function (err, users) {
                if (!err && users) {

                    users.forEach(function (userfound) {

                        friends.forEach(function (friendfound) {
                            if (friendfound.loginId == userfound.loginId) {
                                friendsWithCheckin.push({
                                    name: friendfound.name,
                                    loginId: friendfound.loginId,
                                    profilePic: friendfound.profilePic
                                });
                            }
                        });
                    });
                    if (friendsWithCheckin.length > 0) {
                        res.json({
                            success: true,
                            data: friendsWithCheckin,
                            error: null
                        });
                    } else {
                        res.json({
                            success: true,
                            data: null,
                            error: null
                        });
                    }
                }
            });

        } else {
            res.json({
                success: false,
                data: null,
                error: 'error fetching user info'
            });
        }
    });
});


//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
//                                                                                         SOCIAL ROUTES
//API to get list of friends all
userRoutes.get('/friends', function (req, res) {
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    var friendlist = [];

    function sortFunction(a, b) {
        var name1, name2;
        name1 = a.name;
        name2 = b.name;
        return name1 < name2 ? -1 : 1;
    }

    Users.findOne({token: token}, function (err, userFound) {
        if (!err && userFound) {

            friendlist = userFound.friendsList;
            friendlist.sort(sortFunction);
            res.json({
                success: true,
                data: {
                    friends: friendlist
                },
                error: null
            });
        } else {
            res.json({
                success: false,
                data: null,
                error: 'error fetching friends'
            });
        }

    });


});


//API to patch and update friend status to pending on friend request sent
userRoutes.put('/addsinglefriend', function (req, res) {
    var friendId = req.query.loginId;
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    var now = new Date();

    Users.findOneAndUpdate({token: token, 'friendsList.loginId': friendId}, {
        $set: {
            'friendsList.$.status': 'requested',
            'friendsList.$.updated_at': now
        }
    }, function (err, requestor) {
        if (!err && requestor) {


            Users.findOneAndUpdate({loginId: friendId, 'friendsList.loginId': requestor.loginId}, {
                $set: {
                    'friendsList.$.status': 'pending'
                }

            }, function (err, requestee) {
                if (!err && requestee) {
                    var obj = {};
                    obj.name = null;
                    obj.loginId = null;
                    obj.status = 'pending';
                    obj.profilePic = null;
                    obj.name = requestor.name;
                    obj.loginId = requestor.loginId;
                    obj.profilePic = requestor.profilePic;
                    requestee.friendRequest.push(obj);
                    requestee.save(function (err, friendrequestsaved) {
                        if (!err && friendrequestsaved) {


                            if (!err && friendrequestsaved) {
                                var message = new gcm.Message({
                                    collapseKey: 'demo',
                                    priority: 'high',
                                    contentAvailable: true,
                                    delayWhileIdle: true,
                                    timeToLive: 3,
                                    //restrictedPackageName: "somePackageName",
                                    data: {
                                        type: "addrequest",
                                        title: "New add request!",
                                        body: "You have an add request from " + requestor.name,
                                        friendRequest: requestee.friendRequest
                                    },
                                    notification: {
                                        icon: "app_logo",
                                        sound: "default",
                                        color: "#fbb117"
                                    }
                                });

                                //setting sender registered API KEY
                                var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');
                                var registrationIds = [];
                                //adding sender reg Id (gcmId)
                                registrationIds.push(requestee.gcmId);
                                sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                                    if (!err && result) {
                                        res.json({
                                            success: true,
                                            data: {
                                                message: 'friend request updated both sides'
                                            },
                                            error: null
                                        });
                                    } else {
                                        res.json({
                                            success: false,
                                            data: null,
                                            error: 'didnt send push notification'
                                        });
                                    }
                                });//end of push gcm send request


                            }//end of(if!err and data1)


                        }//end of if(!err && usersaved)

                    });//user friendRequest saved

                }//end of if(!err && data)

            });//end of findone to get details of user to save in requests
        } else console.log(err);
    });//end of user find to save in friendRequest

});//end of userroutes patch API for single friend request


userRoutes.put('/friendaccepted', function (req, res) {
    var friendId = req.query.loginId;
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    console.log(friendId);
    Users.findOneAndUpdate({token: token, 'friendRequest.loginId': friendId}, {
        $pull: {friendRequest: {loginId: friendId}}
    }, function (err, requestupdated) {
        if (!err && requestupdated) {


            Users.findOneAndUpdate({loginId: requestupdated.loginId, 'friendsList.loginId': friendId}, {
                $set: {'friendsList.$.status': 'accepted'}
            }, function (err, accepted) {

                if (!err && accepted) {
                    Users.findOneAndUpdate({loginId: friendId, 'friendsList.loginId': accepted.loginId}, {
                        $set: {'friendsList.$.status': 'accepted'}
                    }, function (err, data) {
                        if (!err && data) {


                            var message = new gcm.Message({
                                collapseKey: 'demo',
                                priority: 'high',
                                contentAvailable: true,
                                delayWhileIdle: true,
                                timeToLive: 3,
                                //restrictedPackageName: "somePackageName",
                                data: {
                                    type: "requestaccepted"
                                },
                                notification: {
                                    title: accepted.name + " accepted your friend request",
                                    icon: "app_logo",
                                    body: "You are now friends with " + accepted.name + ". Come see what they have been upto!",
                                    sound: "default",
                                    color: "#fbb117"
                                }
                            });

                            //setting sender registered API KEY
                            var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');
                            var registrationIds = [];
                            //adding sender reg Id (gcmId)
                            registrationIds.push(data.gcmId);
                            sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                                if (err) console.log(err);
                                else console.log(result);
                            });//end of push gcm send request


                            res.json({
                                success: true,
                                data: {message: 'friend updated to accepted'},
                                error: null
                            });
                        }
                    });

                }
            });
        }//end of if (!err && requestUpdated)
        else console.log(err);
    });//end of findOne of user whose requests have to be updated
});//end of FRIEND accepted API call and pulled from requests


userRoutes.put('/friendrejected', function (req, res) {
    var friendId = req.query.loginId;
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];

    Users.findOneAndUpdate({token: token, 'friendsList.loginId': friendId}, {
        $pull: {friendRequest: {loginId: friendId}}
    }, function (err, requestupdated) {
        if (!err && requestupdated) {

            Users.findOneAndUpdate({loginId: requestupdated.loginId, 'friendsList.loginId': friendId}, {
                $set: {'friendsList.$.status': 'rejected'}
            }, function (err, rejected) {
                if (!err && rejected) {
                    Users.findOneAndUpdate({loginId: friendId, 'friendsList.loginId': rejected.loginId}, {
                        $set: {'friendsList.$.status': 'rejected'}
                    }, function (err, rejected2) {
                        if (!err && rejected2) {
                            res.json({
                                success: true,
                                data: {message: 'friend updated to rejected'},
                                error: null
                            });

                        } else console.log(err);
                    });//end of findOne requestor update friendslist to rejected
                }
                else console.log(err);
            });//end of findOne to update friendsList in requestee list
        } else console.log(err);

    });//end of findOne to pull request at requestee end

});//end of FRIEND accepted API call and pulled from requests


//API for check_in
userRoutes.post('/checkin', function (req, res) {
    var form = new formidable.IncomingForm();
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];

    //HELPER to rewrite received temporary file
    function rewrite(image, path) {
        //reading incoming file
        fs.readFile(image, function (err, data) {
            //writing read file to location on server
            fs.writeFile(path, data, function (err) {
                if (err)
                    console.log('error writing');
                //removing temporary image file
                fs.unlink(image, function (err) {
                    if (err)
                        console.log('error unlinking');

                    else {
                        console.log('success image saved');
                    }
                });//unlinked temp image
            });//writing to new path
        });//reading image file
    }//end of rewrite function
    var check = false;
    var now = new Date();

    form.parse(req, function (err, fields, files) {


        var checkinobj = JSON.parse(fields.checkInModel);

        Users.findOne({token: token}, function (err, userCheckIn) {
            if (!err && userCheckIn) {


                var usernme = userCheckIn.name;
                Place.findOne({placeName: checkinobj.placeName}, function (err, placefound) {
                    if (!err && placefound) {
                        var obj = {};
                        obj.placeName = null;
                        obj.selfieImage = null;
                        obj.time = null;
                        obj.lat = null;
                        obj.long = null;
                        obj.placeName = checkinobj.placeName;
                        obj.time = now;
                        obj.lat = placefound.lat;
                        obj.lon = placefound.lon;
                        userCheckIn.checkIns.push(obj);
                        userCheckIn.save(function (err, saved) {
                            if (!err && saved) {

                                res.json({
                                    success: true,
                                    data: {
                                        message: 'Checked in at ' + obj.placeName + '  TIME: ' + now
                                    },
                                    error: null


                                });

                                if (placefound.selfies != null) {
                                    try {
                                        var counter = placefound.selfies.length;
                                        counter++;
                                        var path = (__dirname + '/uploads/selfies/' + placefound.phoneNumber + 'selfie' + counter + '.jpg');
                                        var selfie = files.selfie.path;
                                        rewrite(selfie, path);
                                        var pathset = ('/selfies/' + placefound.phoneNumber + 'selfie' + counter + '.jpg');
                                        placefound.selfies.push(pathset);
                                        placefound.save(function (err, selfiesaved) {
                                            if (!err && selfiesaved) {
                                                check = true;
                                                Users.findOneAndUpdate({token: token, 'checkIns.time': now}, {
                                                    $set: {'checkIns.$.selfieImage': pathset}
                                                }, function (err, friendCheckinSelfieUpdated) {

                                                    if (!err && friendCheckinSelfieUpdated) {

                                                    }
                                                });
                                            }
                                        });
                                    }
                                    catch (err) {
                                        console.log(err);
                                    }
                                } else {
                                    try {
                                        counter = 1;
                                        path = (__dirname + '/uploads/selfies/' + placefound.phoneNumber + 'selfie' + counter + '.jpg');
                                        selfie = files.selfie.path;
                                        rewrite(selfie, path);
                                        pathset = ('/selfies/' + placefound.phoneNumber + 'selfie' + counter + '.jpg');
                                        placefound.selfies.push(pathset);
                                        placefound.save(function (err, selfiesaved) {
                                            if (!err && selfiesaved) {
                                                Users.findOneAndUpdate({token: token, 'checkIns.time': now}, {
                                                    $set: {'checkIns.$.selfieImage': pathset}
                                                }, function (err, friendCheckinSelfieUpdated) {
                                                    if (!err && friendCheckinSelfieUpdated) {


                                                    }
                                                });
                                            }
                                        });
                                    }
                                    catch (err) {
                                        console.log(err);
                                    }

                                }

                            }
                        });
                    } else console.log(err);
                });


            } else console.log(err);
        });//end of user checkin update
    });//end of form parse
});//end of Checkin API


//API to get friends checkins
userRoutes.get('/friendscheckin', function (req, res) {
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    var friendsfinal = [];
    var check = false;

    function sortFunction(a, b) {
        var dateA = new Date(a.time).getTime();
        var dateB = new Date(b.time).getTime();
        return dateA < dateB ? 1 : -1;
    }

    Users.findOne({token: token}, function (err, userfound) {
        Users.find({}, function (err, found) {

            found.forEach(function (eachfound) {
                if (eachfound.checkIns != null) {
                    userfound.friendsList.forEach(function (friend) {
                        if (friend.loginId == eachfound.loginId) {
                            var array = eachfound.checkIns;
                            //sorting array in descending offer

                            if (array.length > 0) {
                                array.sort(sortFunction);
                                friendsfinal.push({
                                    name: friend.name,
                                    checkIns: array,
                                    profilePic: friend.profilePic,
                                    loginId: friend.loginId
                                });
                                check = true;
                            }

                        }

                    });
                }

            });

            if (check) {
                console.log(friendsfinal);
                if (friendsfinal.length > 0) {
                    res.json({
                        success: true,
                        data: {checkIn: friendsfinal},
                        error: null
                    });
                } else {
                    res.json({
                        success: true,
                        data: null,
                        error: null
                    });
                }
            }

        });
    });
});


//API to find friend details
userRoutes.get('/frienddetails', function (req, res) {
    var friendId = req.query.loginId;
    Users.findOne({loginId: friendId}
        , function (err, friendfound) {
            if (!err && friendfound) {


                function sortFunction(a, b) {
                    var dateA = new Date(a.time).getTime();
                    var dateB = new Date(b.time).getTime();
                    return dateA < dateB ? 1 : -1;
                }

                var array = friendfound.checkIns;
                array.sort(sortFunction);//sorting array in descending offer
                var objoutput = {
                    name: friendfound.name,
                    profilepic: friendfound.profilePic,
                    loginId: friendfound.loginId,
                    likedPlaces: friendfound.likedPlaces,
                    checkIns: array
                };

                res.json({
                    success: true,
                    data: objoutput,
                    error: null
                });

            } else {
                res.json({
                    success: false,
                    data: null,
                    error: 'error fetching friend'
                });
            }
        });//end of findOne for getting friend details
});//end of API route to get friend details


userRoutes.post('/sendinvite', function (req, res) {
    var token = req.body.auth_token || req.query.auth_token || req.headers['auth_token'];
    var friendId = req.body.loginId;
    var placename = req.body.placeName;
    var inviteTime = req.body.inviteTime;
    var inviteDate = req.body.inviteDate;
    var now = new Date();


    Users.findOne({token: token}, function (err, sender) {
        if (!err && sender) {
            Place.findOne({placeName: placename}, function (err, place) {
                if (!err && place) {
                    var sendername = sender.name;

                    var obj = {
                        senderName: sendername,
                        senderImage: sender.profilePic,
                        loginId: sender.loginId,
                        inviteTime: inviteTime,
                        inviteDate: inviteDate,
                        created_at: now,
                        updated_at: now,
                        placeName: place.placeName,
                        placeImage: place.thumbnail,
                        status: 'pending'
                    };
                    Users.findOneAndUpdate({loginId: friendId}, {
                        $push: {inviteRequest: obj}
                    }, function (err, receiver) {
                        if (!err && receiver) {

                            var sendernameinside = sendername;
                            var placeName = place.placeName;
                            var invitetime = inviteTime;
                            var invitedate = inviteDate;
                            var message = new gcm.Message({
                                collapseKey: 'demo',
                                priority: 'high',
                                contentAvailable: true,
                                delayWhileIdle: true,
                                timeToLive: 3,

                                //restrictedPackageName: "somePackageName",
                                data: {
                                    type: "sendinvite",
                                    inviteRequest: obj,
                                    title: sendernameinside + ' invited you to ' + placeName,
                                    body: 'You have been invited to join ' + sendernameinside + ' to ' + placeName + ' at ' + invitetime + 'on' + invitedate
                                },
                                notification: {
                                    icon: "app_logo",
                                    sound: "default",
                                    color: "#fbb117"
                                }
                            });

                            //setting sender registered API KEY
                            var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');
                            var registrationIds = [];
                            //adding sender reg Id (gcmId)
                            registrationIds.push(receiver.gcmId);
                            sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                                if (err) console.log(err);
                                else console.log(result);
                            });//end of push gcm send request

                            res.json({
                                success: true,
                                data: {
                                    message: 'invitation sent'
                                },
                                error: null
                            });

                        } else
                            res.json({
                                success: false,
                                data: null,
                                error: 'error saving request'
                            });
                    });//end of findOne to
                } else console.log(err);
            });//end of findOne to getplace details
        } else console.log(err);

    });//end of findOne to getsender info
});//end of API to send invite


//API to accept user invite
userRoutes.get('/inviteaccept', function (req, res) {
    var token = req.headers['auth_token'];
    var now = new Date();
    var inviteDate = req.body.inviteDate;
    var inviteTime = req.body.inviteTime;
    var senderloginid = req.body.loginId;
    var placeName = req.body.placeName;


    Users.findOneAndUpdate({
        token: token,
        'inviteRequest.inviteTime': inviteTime,
        'inviteRequest.inviteDate': inviteDate,
        'inviteRequest.placeName': placeName,
        'inviteRequest.loginId': senderloginid
    }, {
        $set: {
            'inviteRequest.status': 'going',
            'inviteRequest.updated_at': now
        }
    }, function (err, userinviteupdated) {
        if (!err && userinviteupdated) {
            Users.findOne({loginId: senderloginid}, function (err, userfound) {
                if (!err && userfound) {

                    var message = new gcm.Message({
                        collapseKey: 'demo',
                        priority: 'high',
                        contentAvailable: true,
                        delayWhileIdle: true,
                        timeToLive: 3,

                        //restrictedPackageName: "somePackageName",
                        data: {
                            type: "inviteaccepted",
                            inviteRequest: obj,
                            title: userinviteupdated.name + ' accepted your invite',
                            body: userinviteupdated.name + ' acccepted your invite to ' + placeName + ', on ' + inviteDate + ' at ' + inviteTime
                        },
                        notification: {
                            icon: "app_logo",
                            sound: "default",
                            color: "#fbb117"
                        }
                    });

                    var sender = new gcm.Sender('AIzaSyAQzXaYgOpi_C9LtEwma1GQFVNJR9n3tHQ');
                    var registrationIds = [];
                    registrationIds.push(userfound.gcmId);
                    sender.send(message, {registrationIds: registrationIds}, function (err, result) {
                        if (!err && result) {
                            console.log(result);
                        } else
                            console.log(err);
                    });

                    res.json({
                        success: true,
                        data: {
                            message: 'invite accepted'
                        },
                        error: null
                    });
                }
            });
        }
        else {
            res.json({
                success: false,
                data: null,
                error: 'error updating invite'
            });
        }

    });
});//end of invite accept API


//API to reject userinvite
userRoutes.get('/invitereject', function (req, res) {
    var token = req.headers['auth_token'];
    var inviteDate = req.body.inviteDate;
    var inviteTime = req.body.inviteTime;
    var senderloginid = req.body.loginId;
    var placeName = req.body.placeName;


    Users.findOneAndUpdate({
        token: token,
        'inviteRequest.inviteTime': inviteTime,
        'inviteRequest.inviteDate': inviteDate
    }, {
        $pull: {
            inviteRequest: {
                inviteDate: inviteDate,
                inviteTime: inviteTime,
                loginId: senderloginid,
                placeName: placeName
            }
        }
    }, function (err, userinviteupdated) {
        if (!err && userinviteupdated) {
            res.jon({
                success: true,
                data: {
                    message: 'invite rejected'
                },
                error: null
            });
        }
        else
            res.json({
                success: false,
                data: null,
                error: 'error rejecting invite'
            });
    });
});

//API to fetch all of users requests
userRoutes.get('/inviteslist', function (req, res) {
    var token = req.headers['auth_token'];

    Users.findOne({token: token}, function (err, userfound) {
        if (!err && userfound) {
            var invites = userfound.inviteRequest;
            res.json({
                success: true,
                data: {
                    invites: invites
                },
                error: null
            });
        } else
            res.json({
                success: false,
                data: null,
                error: 'error occured while fetching invites'
            });
    });
});//end of fetching user invites list API


//API for fetching My likedPlaces
userRoutes.get('/mylikes', function (req, res) {
    var token = req.header.auth_token;
    Users.findOne({token: token}, {_id: 0, likedPlaces: 1}, function (err, userfound) {
        if (!err && userfound) {
            if (userfound.likedPlaces.length > 0) {

                res.json({
                    success: true,
                    data: userfound.likedPlaces,
                    error: null
                });
            } else {
                res.json({
                    success: true,
                    data: null,
                    error: null
                });
            }
        } else {
            res.json({
                success: false,
                data: null,
                error: ' User not found'
            });

        }
    });
});//end of API for fetching My likedPlaces




userRoutes.get('/locdistance', function (req, res) {
    var token = req.headers['auth_token'];
    var latcurrent = req.body.lat;
    var loncurrent = req.body.lon;
    var array = [];
    var objcurrent = {
        latitude: parseFloat(latcurrent),
        longitude: parseFloat(loncurrent)
    };

    function getNearbyHappyHours(hh, callback) {


        hh.forEach(function (hheach) {

            var distance;
            var latdestination = parseFloat(hheach.places[0].lat);
            var londestination = parseFloat(hheach.places[0].lon);
            var objdestination = {
                latitutde: latdestination,
                longitude: londestination
            };
            distance = geolib.getDistance(objcurrent, objdestination, 10);
            console.log(distance);
            array.push(distance);
        });
        callback(array);
    }

    HH.find({}, function (err, hhfound) {
        if (!err && hhfound) {
            getNearbyHappyHours(hhfound, function (data) {

                if (data) {
                    res.json({
                        success: true,
                        data: data,
                        error: null
                    });
                }
            });
        } else {
            res.json({
                success: false,
                data: null,
                error: 'An error occured fetching places'
            });
        }
    });
});


//SETTING up ROUTES and starting server
app.use('/user', userRoutes);


app.use('/admin', adminRoutes);


app.listen(port);
console.log('Server listening at http://localhost:' + port);
