var mongoose =  require('mongoose');
var Schema = mongoose.Schema;
var Admin = new Schema({
    userName: String,
    password: String,
    token: String
});

module.exports = mongoose.model('Admin', Admin);