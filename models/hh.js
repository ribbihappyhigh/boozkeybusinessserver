var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var HH = new Schema({

    Places: [{
        placeName: String,
        placeImage: String,
        checked: Boolean,
        lat: String,
        lon: String

    }],
    happyHourDescription: String,
    happyHourDays: [],
    happyHourStartTime: String,
    happyHourEndTime: String,
    happyHourAlcoholType: []


});

module.exports = mongoose.model('HH', HH);