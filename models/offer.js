var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Offer = new Schema({

    offerName: String,
    offerDetail: String,
    offerDays: [],
    startTime: String,
    endTime: String,
    offerImage: String,
    offerAlcoholType: [],
    Places:[{
        placeName: String,
        placeImage: String,
        checked: Boolean


    }]
});

module.exports = mongoose.model('Offer', Offer);