var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Place = new Schema({

    placeName: String,
    phoneNumber: String,
    address: String,

    startTime: String,
    endTime: String,
    big: String,
    thumbnail: String,
    menu1: String,
    menu2: String,
    menu3: String,
    selfies: [],
    lat: String,
    lon: String


});

module.exports = mongoose.model('Place', Place);