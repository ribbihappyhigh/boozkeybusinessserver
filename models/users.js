var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Users = new Schema({
    loginId: String,
    name: String,
    gender: String,
    birthday: String,
    email: String,
    profilePic: String,
    gcmId: String,
    token: String,
    homelocation: {
        lat: String,
        lon: String
    },
    friendsList: [{
        loginId: String,
        name: String,
        profilePic: String,
        status: String,
        created_at: Date,
        updated_at: Date
    }],
    likedPlaces: [{
        placeName: String,
        placeImage: String
    }],
    friendRequest: [],
    inviteRequest: [{
        senderName: String,
        senderImage: String,
        loginId: String,
        inviteTime: String,
        inviteDate: String,
        placeName: String,
        placeImage: String,
        created_at: String,
        updated_at: String,
        status: String
    }],
    checkIns: [{
        placeName: String,
        selfieImage: String,
        time: Date,
        lat: String,
        lon: String
    }],
    created_at: Date,
    updated_at: Date

});

module.exports = mongoose.model('Users', Users);